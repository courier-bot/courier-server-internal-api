# frozen_string_literal: true

json.call(
  record,
  :id,
  :message_kind_id,
  :target_messageable,
  :status,
  :created_at,
  :updated_at
)
