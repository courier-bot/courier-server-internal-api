# frozen_string_literal: true

json.call(
  record,
  :id,
  :delivery_id,
  :name,
  :value,
  :created_at,
  :updated_at
)
