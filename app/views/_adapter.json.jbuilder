# frozen_string_literal: true

json.call(
  record,
  :id,
  :code,
  :uri,
  :created_at,
  :updated_at,
  :deleted_at
)
