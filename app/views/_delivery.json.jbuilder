# frozen_string_literal: true

json.call(
  record,
  :id,
  :message_id,
  :delivery_kind_id,
  :external_id,
  :status,
  :undelivered_reason,
  :created_at,
  :updated_at
)
