# frozen_string_literal: true

json.call(
  record,
  :id,
  :code,
  :delivery_method_id,
  :adapter_id,
  :created_at,
  :updated_at,
  :deleted_at
)
