# frozen_string_literal: true

json.call(
  record,
  :id,
  :code,
  :created_at,
  :updated_at,
  :deleted_at
)
