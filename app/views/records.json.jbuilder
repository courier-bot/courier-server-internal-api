# frozen_string_literal: true

json.partial!(
  @template,
  collection: @records,
  as: :record
)
