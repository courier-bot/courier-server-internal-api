# frozen_string_literal: true

json.call(
  record,
  :id,
  :code,
  :adapter_id,
  :created_at,
  :updated_at,
  :deleted_at
)
