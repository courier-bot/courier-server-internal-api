# frozen_string_literal: true

json.partial!(
  @template,
  record: @record
)
