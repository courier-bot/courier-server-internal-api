# frozen_string_literal: true

module ApiErrors
  # Represents an HTTP 422 UNPROCESSABLE ENTITY error.
  class UnprocessableEntityError < ClientApiError
    # Initializes an API error with the error code and some details.
    def initialize(code, detail)
      super(
        422,
        '422 UNPROCESSABLE ENTITY',
        code,
        'Invalid attribute value',
        detail
      )
    end
  end
end
