# frozen_string_literal: true

module ApiErrors
  # Represents an HTTP 400 BAD REQUEST error.
  class BadRequestError < ClientApiError
    # Initializes an API error with the error code and some details.
    def initialize(code, title, detail)
      super(
        400,
        '400 BAD REQUEST',
        code,
        title,
        detail
      )
    end
  end
end
