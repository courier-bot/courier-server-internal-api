# frozen_string_literal: true

module ApiErrors
  # Represents an HTTP 404 NOT FOUND error.
  class NotFoundError < ClientApiError
    # Initializes an API error with the error code and some details.
    def initialize(code, detail)
      super(
        404,
        '404 NOT FOUND',
        code,
        'Resource not found',
        detail
      )
    end
  end
end
