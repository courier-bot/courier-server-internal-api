# frozen_string_literal: true

module ApiErrors
  # Represents a generic HTTP request error. Attributes are based on the JSON-API error objects
  # defined in https://jsonapi.org/format/#error-objects.
  class ApiError < StandardError
    attr_reader :http_status_code,
                :status,
                :code,
                :title,
                :detail

    # Initializes an API error with all necessary attributes.
    #
    # @param http_status_code [Integer] The HTTP status code, eg. `404`.
    # @param status [String] The HTTP status code applicable to this problem, expressed as a string
    #   value.
    # @param code [String] An application-specific error code, expressed as a string value.
    # @param title [String] A short, human-readable summary of the problem that SHOULD NOT change
    #   from occurrence to occurrence of the problem, except for purposes of localization.
    # @param detail [String] A human-readable explanation specific to this occurrence of the
    #   problem. Like `title`, this field's value can be localized.
    def initialize(http_status_code, status, code, title, detail)
      @http_status_code = http_status_code
      @status = status
      @code = code
      @title = title
      @detail = detail
    end

    # Builds a hash containing relevant information about the current error. The contents are based
    # on the JSON-API error objects.
    def json_api_error_object
      {
        status: status,
        code: code,
        title: title,
        detail: detail
      }
    end
  end
end
