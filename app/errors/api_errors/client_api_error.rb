# frozen_string_literal: true

module ApiErrors
  # Represents an HTTP 4XX error.
  class ClientApiError < ApiError; end
end
