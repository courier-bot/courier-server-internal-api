# frozen_string_literal: true

# Handles requests related to deliveries.
class DeliveriesController < RecordsController
  protected

  def record_class
    Delivery
  end

  def record_url(delivery)
    delivery_url(delivery)
  end

  def validate_params(params)
    status = params.fetch('status')

    return if Delivery.statuses.key?(status)

    raise resource_invalid(Delivery.model_name, 'status', status)
  end

  def creatable_attributes
    %i[message_id delivery_kind_id external_id status]
  end

  def modifiable_attributes
    %i[delivery_kind_id external_id status undelivered_reason]
  end

  def filterable_attributes
    %i[message_id]
  end

  def sortable_attributes
    %i[message_id delivery_kind_id status]
  end
end
