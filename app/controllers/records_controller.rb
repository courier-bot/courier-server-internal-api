# frozen_string_literal: true

# Base controller class for records.
class RecordsController < ApplicationController
  attr_reader :template

  # Initializes some generic data about the records controller.
  def initialize
    @template = record_template
  end

  # Handles `GET /records` requests to retrieve multiple records.
  #
  # The response body will contain a JSON with the records.
  def index
    @records = all_records.where(filtered_attributes).order(sorted_attributes)

    render '/records',
           formats: :json
  end

  # Handles `POST /records` requests to create a record.
  #
  # The response body will contain a JSON with the new record.
  #
  # @raise [ApiErrors::UnprocessableEntityError] If the record attributes are invalid.
  def create
    @record = new_record

    params = permitted_creation_params
    validate_params(params)

    @record.assign_attributes(params)
    validate_record(@record)

    @record.save!

    headers['Location'] = record_url(@record)

    render '/record',
           formats: :json,
           status: :created
  end

  # Handles `GET /records/:id` requests to retrieve a record.
  #
  # The response body will contain a JSON with the record.
  #
  # @raise [ApiErrors::NotFoundError] If the record id is invalid.
  def show
    @record = requested_record

    render '/record',
           formats: :json
  end

  # Handles `PUT|PATCH /records/:id` requests to update a record.
  #
  # The response body will contain a JSON with the new record.
  #
  # @raise [ApiErrors::NotFoundError] If the record id is invalid.
  # @raise [ApiErrors::UnprocessableEntityError] If the record attributes are invalid.
  def update
    @record = requested_record

    params = permitted_modification_params
    validate_params(params)

    @record.assign_attributes(params)
    validate_record(@record)

    @record.save!

    render '/record',
           formats: :json
  end

  # Handles `DELETE /records/:id` requests to delete a record.
  #
  # An HTTP 204 NO CONTENT will be returned.
  #
  # @raise [ApiErrors::BadRequestError] If the record is still linked by foreign key.
  # @raise [ApiErrors::NotFoundError] If the record id is invalid.
  def destroy
    record = requested_record
    record.destroy!
  rescue ActiveRecord::InvalidForeignKey
    raise resource_not_deleted(record_class.model_name)
  end

  protected

  def validate_params(_params); end

  def validate_record(record)
    return if record.valid?

    (attribute, details) = record.errors.details.first
    details = details.first

    raise resource_invalid(record_class.model_name, attribute, details[:value])
  end

  def permitted_creation_params
    attrs = creatable_attributes.map { |attr| attr.to_s.camelcase(:lower) }
    params.permit(attrs).transform_keys(&:underscore)
  end

  def permitted_modification_params
    attrs = modifiable_attributes.map { |attr| attr.to_s.camelcase(:lower) }
    params.permit(attrs).transform_keys(&:underscore)
  end

  def all_records
    record_class.all
  end

  def requested_record
    record = all_records.find_by(id: requested_id)

    raise resource_not_found(record_class.model_name) if record.nil?

    record
  end

  def new_record
    all_records.new
  end

  def requested_id
    params.require(:id)
  end

  def record_template
    "/#{record_class.model_name.param_key}"
  end

  private

  # Query string parameters will determine the filters. Reserved parameter keys will be determined
  # at a later date to accomodate sorting and paging of results.
  #
  # Eg. `...?code=my_code&name=my_name`.
  def filtered_attributes
    attributes = filterable_attributes.map { |attr| attr.to_s.camelcase(:lower) }

    params
      .permit(attributes)
      .transform_keys(&:underscore)
  end

  # The `sort` query string parameter will determine the record ordering. Multiple attributes can
  # be chained together with a comma. Ascending order is the default, but can be specified with
  # a `+` character. Descending order requires the `-` character prefix.
  #
  # Eg. `...?sort=name,-createdAt`.
  def sorted_attributes
    params
      .fetch('sort') { '' }
      .split(',')
      .each_with_object({}) do |field, result|
        field.prepend('+') unless field.first.in?('+-')

        direction = field.first == '-' ? :desc : :asc
        field = field[1..-1].underscore.to_sym

        next unless field.in?(sortable_attributes)

        result[field] = direction
      end
  end

  def resource_not_found(model_name)
    ApiErrors::NotFoundError.new(
      "#{model_name.param_key}_not_found",
      "#{model_name.human} with id `#{requested_id}` could not be found."
    )
  end

  def resource_not_deleted(model_name)
    ApiErrors::BadRequestError.new(
      "#{model_name.param_key}_not_deleted",
      'Resource not deleted',
      "#{model_name.human} with id `#{requested_id}` could not be deleted."
    )
  end

  def resource_invalid(model_name, attribute_name, value)
    ApiErrors::UnprocessableEntityError.new(
      "#{model_name.param_key}_#{attribute_name}_invalid",
      "#{model_name.human} attribute `#{attribute_name}` with value `#{value}` is invalid."
    )
  end
end
