# frozen_string_literal: true

# Handles requests related to delivery methods.
class DeliveryMethodsController < RecordsController
  protected

  def record_class
    DeliveryMethod
  end

  def record_url(delivery_method)
    delivery_method_url(delivery_method)
  end

  def creatable_attributes
    %i[code]
  end

  def modifiable_attributes
    %i[code deleted_at]
  end

  def filterable_attributes
    %i[code]
  end

  def sortable_attributes
    %i[code created_at]
  end
end
