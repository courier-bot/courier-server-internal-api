# frozen_string_literal: true

# Handles requests related to messages.
class MessagesController < RecordsController
  protected

  def record_class
    Message
  end

  def record_url(message)
    message_url(message)
  end

  def validate_params(params)
    status = params.fetch('status')

    return if Message.statuses.key?(status)

    raise resource_invalid(Message.model_name, 'status', status)
  end

  def creatable_attributes
    %i[message_kind_id target_messageable status]
  end

  def modifiable_attributes
    %i[message_kind_id target_messageable status undelivered_reason]
  end

  def filterable_attributes
    %i[status]
  end

  def sortable_attributes
    %i[message_kind_id target_messageable status created_at]
  end
end
