# frozen_string_literal: true

# Handles requests related to delivery kinds.
class DeliveryKindsController < RecordsController
  protected

  def record_class
    DeliveryKind
  end

  def record_url(delivery_kind)
    delivery_kind_url(delivery_kind)
  end

  def creatable_attributes
    %i[code delivery_method_id adapter_id]
  end

  def modifiable_attributes
    %i[code delivery_method_id adapter_id deleted_at]
  end

  def filterable_attributes
    %i[code delivery_method_id]
  end

  def sortable_attributes
    %i[code delivery_method_id created_at]
  end
end
