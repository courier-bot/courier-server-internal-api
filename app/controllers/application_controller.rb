# frozen_string_literal: true

# Base controller class for the application.
class ApplicationController < ActionController::API
  rescue_from ApiErrors::ApiError, with: :api_error

  protected

  def api_error(error)
    render json: error.json_api_error_object,
           status: error.http_status_code
  end
end
