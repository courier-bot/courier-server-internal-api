# frozen_string_literal: true

# Handles requests related to adapters.
class AdaptersController < RecordsController
  protected

  def record_class
    Adapter
  end

  def record_url(adapter)
    adapter_url(adapter)
  end

  def creatable_attributes
    %i[code uri]
  end

  def modifiable_attributes
    %i[code uri deleted_at]
  end

  def filterable_attributes
    %i[code]
  end

  def sortable_attributes
    %i[code created_at]
  end
end
