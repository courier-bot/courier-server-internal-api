# frozen_string_literal: true

# Handles requests related to message kinds.
class MessageKindsController < RecordsController
  protected

  def record_class
    MessageKind
  end

  def record_url(message_kind)
    message_kind_url(message_kind)
  end

  def creatable_attributes
    %i[code adapter_id]
  end

  def modifiable_attributes
    %i[code adapter_id deleted_at]
  end

  def filterable_attributes
    %i[code]
  end

  def sortable_attributes
    %i[code adapter_id created_at]
  end
end
