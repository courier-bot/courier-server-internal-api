# frozen_string_literal: true

# Handles requests related to delivery fields.
class DeliveryFieldsController < RecordsController
  protected

  def record_class
    DeliveryField
  end

  def record_url(delivery_field)
    delivery_field_url(delivery_field)
  end

  def creatable_attributes
    %i[delivery_id name value]
  end

  def modifiable_attributes
    %i[value]
  end

  def filterable_attributes
    %i[delivery_id]
  end

  def sortable_attributes
    %i[name]
  end
end
