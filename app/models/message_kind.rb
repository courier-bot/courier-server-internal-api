# frozen_string_literal: true

# Represents a kind of message, or what happened to justify sending a message. This can be anything,
# and will determine the content of the message.
class MessageKind < ApplicationRecord
  # Code that uniquely describes a message kind. This is used to find the correct application client
  # to connect with, when asking for delivery information.
  attribute :code, :string
  validates :code,
            length: { maximum: 100 },
            uniqueness: true,
            presence: true

  # Which adapter the delivery information should be requested from.
  belongs_to :adapter

  attribute :created_at, :datetime
  attribute :updated_at, :datetime
  attribute :deleted_at, :datetime
end
