# frozen_string_literal: true

# Represents a way to contact someone or something, eg. an email or a text message.
class DeliveryMethod < ApplicationRecord
  # Code that uniquely describes the delivery method.
  attribute :code, :string
  validates :code,
            length: { maximum: 100 },
            uniqueness: true,
            presence: true

  attribute :created_at, :datetime
  attribute :updated_at, :datetime
  attribute :deleted_at, :datetime
end
