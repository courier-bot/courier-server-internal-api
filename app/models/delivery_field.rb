# frozen_string_literal: true

# Represents a key-value used to deliver a message, eg. `'from': 'from@example.com'`
class DeliveryField < ApplicationRecord
  # Which delivery this field is for.
  belongs_to :delivery

  # Name of the field, eg. `'from'` or `'to'`.
  attribute :name, :string
  validates :name,
            length: { maximum: 100 },
            presence: true

  # Value of the field, eg. an email address.
  attribute :value, :string
  validates :value,
            length: { maximum: 1024 }

  attribute :created_at, :datetime
  attribute :updated_at, :datetime
end
