# frozen_string_literal: true

# Represents a way to contact someone or something by means of a specific third-party service.
class DeliveryKind < ApplicationRecord
  # Code that uniquely describes the delivery kind.
  attribute :code, :string
  validates :code,
            length: { maximum: 100 },
            uniqueness: true,
            presence: true

  # Delivery method via which the message is to be delivered. This is the `delivery_method` value
  # that application clients will specify when requesting a message to be delivered, eg. `'email'`.
  belongs_to :delivery_method

  # Which adapter the delivery should be requested from.
  belongs_to :adapter

  attribute :created_at, :datetime
  attribute :updated_at, :datetime
  attribute :deleted_at, :datetime
end
