# frozen_string_literal: true

# Represents an adapter that the Courier Server can send a request to. An adapter can either be a
# third-party service that will deliver a message, or an application client that can return
# information about a delivery.
class Adapter < ApplicationRecord
  # Code that uniquely describes an adapter.
  attribute :code, :string
  validates :code,
            length: { maximum: 100 },
            uniqueness: true,
            presence: true

  # URI where the server can POST a request.
  attribute :uri, :string
  validates :uri,
            length: { maximum: 250 },
            presence: true

  attribute :created_at, :datetime
  attribute :updated_at, :datetime
  attribute :deleted_at, :datetime
end
