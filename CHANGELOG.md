# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Created initial models `Message`, `MessageKind`, `Adapter`, `Delivery`, `DeliveryKind`, and
  `DeliveryField`.
- Created controllers for initial models.
- Added HTTP 404 NOT FOUND error handling.
- Added HTTP 400 BAD REQUEST error handling.
