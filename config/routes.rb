# frozen_string_literal: true

Rails.application.routes.draw do
  resources :messages, only: %i[index create show update destroy]
  resources :deliveries, only: %i[index create show update destroy]
  resources :delivery_fields, path: 'deliveryFields', only: %i[index create show update destroy]

  resources :adapters, only: %i[index create show update destroy]
  resources :message_kinds, path: 'messageKinds', only: %i[index create show update destroy]
  resources :delivery_methods, path: 'deliveryMethods', only: %i[index create show update destroy]
  resources :delivery_kinds, path: 'deliveryKinds', only: %i[index create show update destroy]
end
