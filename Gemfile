# frozen_string_literal: true

source 'https://rubygems.org'

ruby '2.6.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.3'

# Use postgresql as the database for Active Record
gem 'pg'

# Use Puma as the app server
gem 'puma'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder'

# sprockets 4.0.0 has a path traversal vulnerability (CVE-2018-3760).
# Upgrade to sprockets 4.0.0.beta8 or newer
gem 'sprockets', '4.0.0.beta8'

group :development do
  # Allows the server to automatically stay updated to changes in the code.
  gem 'listen', '>= 3.0.5', '< 3.2'
end

group :test do
  # Builds fake models based on templates.
  gem 'factory_bot_rails'

  # Generates fake values.
  gem 'faker'

  # Allows for mutation testing. This gem is handled by bundler because it needs to know about
  # `rspec` and all other gem versions.
  gem 'mutant-rspec', require: false

  # Allows for general testing. All tests are in the `spec/` folder.
  gem 'rspec-rails'

  # Adds many Rails-related spec matchers.
  gem 'shoulda'

  # Allows for code coverage during testing. This is included in `spec/rails_helper.rb`.
  gem 'simplecov', require: false
end
