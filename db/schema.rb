# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_28_120738) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "adapters", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "code", limit: 100, null: false
    t.string "uri", limit: 250, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["code"], name: "index_adapters_on_code"
  end

  create_table "deliveries", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "external_id", limit: 250
    t.string "status", limit: 100, null: false
    t.string "undelivered_reason", limit: 4096
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "delivery_kind_id"
    t.uuid "message_id"
    t.index ["delivery_kind_id"], name: "index_deliveries_on_delivery_kind_id"
    t.index ["external_id"], name: "index_deliveries_on_external_id"
    t.index ["message_id"], name: "index_deliveries_on_message_id"
    t.index ["status"], name: "index_deliveries_on_status"
  end

  create_table "delivery_fields", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "name", limit: 100, null: false
    t.string "value", limit: 1024
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "delivery_id"
    t.index ["delivery_id"], name: "index_delivery_fields_on_delivery_id"
  end

  create_table "delivery_kinds", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "code", limit: 100, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.uuid "delivery_method_id"
    t.uuid "adapter_id"
    t.index ["code"], name: "index_delivery_kinds_on_code"
  end

  create_table "delivery_methods", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "code", limit: 100, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["code"], name: "index_delivery_methods_on_code"
  end

  create_table "message_kinds", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "code", limit: 100, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.uuid "adapter_id"
    t.index ["adapter_id"], name: "index_message_kinds_on_adapter_id"
    t.index ["code"], name: "index_message_kinds_on_code"
  end

  create_table "messages", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "target_messageable", limit: 250, null: false
    t.string "status", limit: 100, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "message_kind_id"
    t.index ["message_kind_id"], name: "index_messages_on_message_kind_id"
    t.index ["status"], name: "index_messages_on_status"
    t.index ["target_messageable"], name: "index_messages_on_target_messageable"
  end

  add_foreign_key "deliveries", "delivery_kinds"
  add_foreign_key "deliveries", "messages"
  add_foreign_key "delivery_fields", "deliveries"
  add_foreign_key "delivery_kinds", "adapters"
  add_foreign_key "delivery_kinds", "delivery_methods"
  add_foreign_key "message_kinds", "adapters"
  add_foreign_key "messages", "message_kinds"
end
