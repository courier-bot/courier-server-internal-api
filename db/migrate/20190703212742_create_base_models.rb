class CreateBaseModels < ActiveRecord::Migration[5.2]
  def change
    create_table :clients do |t|
      t.string :code, limit: 100, null: false, index: true
      t.string :uri, limit: 250, null: false
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
      t.datetime :deleted_at
    end
  
    create_table :message_kinds do |t|
      t.string :code, limit: 100, null: false, index: true
      t.references :client, foreign_key: true
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
      t.datetime :deleted_at
    end

    create_table :messages do |t|
      t.references :message_kind, foreign_key: true
      t.string :target_messageable, limit: 250, null: false, index: true
      t.string :status, limit: 100, null: false, index: true
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
    end
      
    create_table :delivery_kinds do |t|
      t.string :code, limit: 100, null: false, index: true
      t.string :contact_kind, limit: 100, null: false
      t.string :handler_type, limit: 100, null: false
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
      t.datetime :deleted_at
    end

    create_table :deliveries do |t|
      t.references :message, foreign_key: true
      t.references :delivery_kind, foreign_key: true
      t.string :external_id, limit: 250, index: true
      t.string :status, limit: 100, null: false, index: true
      t.string :undelivered_reason, limit: 4096
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
    end
  
    create_table :delivery_fields do |t|
      t.references :delivery, foreign_key: true
      t.string :name, limit: 100, null: false
      t.string :value, limit: 1024
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
    end
  end
end
