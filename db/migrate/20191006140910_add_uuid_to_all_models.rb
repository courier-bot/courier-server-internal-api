class AddUuidToAllModels < ActiveRecord::Migration[5.2]
  # Ugly code, but this is the beginning of the application... Normally, this would be done
  # incrementally during a few deployments.
  def up
    # From this point forward, the application is coupled to Postgesql to enable UUIDs.
    enable_extension 'uuid-ossp'

    tables = %i[messages deliveries delivery_fields message_kinds clients delivery_kinds]

    # STEP 1 - Creates all new `uuid` columns with intended values.

    tables.each do |table|
      add_column table, :uuid, :uuid, null: false, default: 'uuid_generate_v4()'
    end

    add_column :deliveries, :delivery_kind_uuid, :uuid
    add_column :deliveries, :message_uuid, :uuid
    add_column :delivery_fields, :delivery_uuid, :uuid
    add_column :message_kinds, :client_uuid, :uuid
    add_column :messages, :message_kind_uuid, :uuid

    execute 'UPDATE deliveries d SET delivery_kind_uuid = dk.uuid FROM delivery_kinds dk WHERE dk.id = d.delivery_kind_id;'
    execute 'UPDATE deliveries d SET message_uuid = m.uuid FROM messages m WHERE m.id = d.message_id;'
    execute 'UPDATE delivery_fields df SET delivery_uuid = d.uuid FROM deliveries d WHERE d.id = df.delivery_id;'
    execute 'UPDATE message_kinds mk SET client_uuid = c.uuid FROM clients c WHERE c.id = mk.client_id;'
    execute 'UPDATE messages m SET message_kind_uuid = mk.uuid FROM message_kinds mk WHERE mk.id = m.message_kind_id;'

    # STEP 2 - Removes the old `id` columns.

    remove_foreign_key :deliveries, :delivery_kinds
    remove_foreign_key :deliveries, :messages
    remove_foreign_key :delivery_fields, :deliveries
    remove_foreign_key :message_kinds, :clients
    remove_foreign_key :messages, :message_kinds

    tables.each do |table|
      execute "ALTER TABLE #{table} DROP CONSTRAINT #{table}_pkey;"
      remove_column table, :id
    end

    remove_column :deliveries, :delivery_kind_id
    remove_column :deliveries, :message_id
    remove_column :delivery_fields, :delivery_id
    remove_column :message_kinds, :client_id
    remove_column :messages, :message_kind_id

    # STEP 3 - Use the `uuid` columns as new primary keys.

    tables.each do |table|
      rename_column table, :uuid, :id
      add_index table, :id, unique: true, name: "#{table}_pkey"
      execute "ALTER TABLE #{table} ADD CONSTRAINT #{table}_pkey PRIMARY KEY USING INDEX #{table}_pkey;"
    end

    rename_column :deliveries, :delivery_kind_uuid, :delivery_kind_id
    rename_column :deliveries, :message_uuid, :message_id
    rename_column :delivery_fields, :delivery_uuid, :delivery_id
    rename_column :message_kinds, :client_uuid, :client_id
    rename_column :messages, :message_kind_uuid, :message_kind_id

    # STEP 4 - Cleans everything up.

    add_index :deliveries, :delivery_kind_id
    add_index :deliveries, :message_id
    add_index :delivery_fields, :delivery_id
    add_index :message_kinds, :client_id
    add_index :messages, :message_kind_id
    
    add_foreign_key :deliveries, :delivery_kinds
    add_foreign_key :deliveries, :messages
    add_foreign_key :delivery_fields, :deliveries
    add_foreign_key :message_kinds, :clients
    add_foreign_key :messages, :message_kinds
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
