class CreateAdapters < ActiveRecord::Migration[5.2]
  def change
    remove_foreign_key :message_kinds, :clients

    rename_table :clients, :adapters
    rename_column :message_kinds, :client_id, :adapter_id
    add_foreign_key :message_kinds, :adapters

    remove_column :delivery_kinds, :handler_type, :string, limit: 100, null: false, default: 'UNDEFINED'
    add_column :delivery_kinds, :adapter_id, :uuid
    add_foreign_key :delivery_kinds, :adapters
  end
end
