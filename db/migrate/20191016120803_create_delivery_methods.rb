class CreateDeliveryMethods < ActiveRecord::Migration[5.2]
  def up
    create_table :delivery_methods, id: :uuid, default: -> { "uuid_generate_v4()" } do |t|
      t.string :code, limit: 100, null: false, index: true
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
      t.datetime :deleted_at
    end

    remove_column :delivery_kinds, :contact_kind, :string, limit: 100, null: false

    add_column :delivery_kinds, :delivery_method_id, :uuid
    add_foreign_key :delivery_kinds, :delivery_methods
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
