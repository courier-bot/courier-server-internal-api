# INTERNAL API

This server offers an interface to the database. It is meant to be a private component of Courier
Server and the only component that can access the database.
