# frozen_string_literal: true

FactoryBot.define do
  factory :adapter do
    code { Faker::String.random.tr("\u0000", '') }
    uri { Faker::Internet.url }
  end
end
