# frozen_string_literal: true

FactoryBot.define do
  factory :delivery_kind do
    code { Faker::String.random.tr("\u0000", '') }
    delivery_method
    adapter
  end
end
