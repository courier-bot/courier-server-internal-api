# frozen_string_literal: true

FactoryBot.define do
  factory :delivery_method do
    code { Faker::String.random.tr("\u0000", '') }
  end
end
