# frozen_string_literal: true

RSpec.describe DeliveryKind, type: :model do
  subject { FactoryBot.create(:delivery_kind) }

  describe 'attributes' do
    describe '#code' do
      it { is_expected.to validate_length_of(:code).is_at_most(100) }
      it { is_expected.to validate_uniqueness_of(:code) }
      it { is_expected.to validate_presence_of(:code) }
    end

    describe '#adapter_id' do
      it { is_expected.to belong_to(:adapter) }
    end
  end

  describe 'associations' do
    describe '#delivery_method' do
      it { is_expected.to belong_to(:delivery_method) }
    end
  end
end
