# frozen_string_literal: true

RSpec.describe Delivery, type: :model do
  subject { delivery }

  let(:delivery) { FactoryBot.create(:delivery) }

  describe 'attributes' do
    describe '#external_id' do
      it { is_expected.to validate_length_of(:external_id).is_at_most(250) }
    end

    describe '#status' do
      it { is_expected.to validate_presence_of(:status) }
    end

    describe '#undelivered_reason' do
      it { is_expected.to validate_length_of(:undelivered_reason).is_at_most(4096) }
    end
  end

  describe 'associations' do
    describe '#message' do
      it { is_expected.to belong_to(:message).dependent(false) }
    end

    describe '#delivery_kind' do
      it { is_expected.to belong_to(:delivery_kind).dependent(false) }
    end

    describe '#delivery_fields' do
      it { is_expected.to have_many(:delivery_fields).dependent(:destroy) }
    end
  end
end
