# frozen_string_literal: true

RSpec.describe MessageKind, type: :model do
  subject { FactoryBot.create(:message_kind) }

  describe 'attributes' do
    describe '#code' do
      it { is_expected.to validate_length_of(:code).is_at_most(100) }
      it { is_expected.to validate_uniqueness_of(:code) }
      it { is_expected.to validate_presence_of(:code) }
    end
  end

  describe 'associations' do
    describe '#adapter' do
      it { is_expected.to belong_to(:adapter) }
    end
  end
end
