# frozen_string_literal: true

RSpec.describe Message, type: :model do
  subject(:message) { instance }

  let(:instance) { FactoryBot.create(:message, status: 'pending') }

  describe 'attributes' do
    describe '#target_messageable' do
      it { is_expected.to validate_length_of(:target_messageable).is_at_least(1) }
      it { is_expected.to validate_length_of(:target_messageable).is_at_most(250) }
      it { is_expected.to validate_presence_of(:target_messageable) }
    end

    describe '#status' do
      it { is_expected.to validate_presence_of(:status) }
    end
  end

  describe 'associations' do
    describe '#message_kind' do
      it { is_expected.to belong_to(:message_kind) }
    end

    describe '#deliveries' do
      it { is_expected.to have_many(:deliveries).dependent(:destroy) }
    end
  end
end
