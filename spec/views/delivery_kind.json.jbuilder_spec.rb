# frozen_string_literal: true

RSpec.describe 'delivery_kind_json_jbuilder', type: :view do
  subject(:rendered_view) { render '/delivery_kind', record: delivery_kind }

  let(:delivery_kind) do
    FactoryBot.create(
      :delivery_kind,
      code: 'delivery_kind_code',
      delivery_method: delivery_method,
      adapter: adapter,
      created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
      updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
      deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
    )
  end

  let(:delivery_method) { FactoryBot.create(:delivery_method) }

  let(:adapter) { FactoryBot.create(:adapter) }

  let(:expected_attributes) do
    {
      'id' => delivery_kind.id,
      'code' => 'delivery_kind_code',
      'deliveryMethodId' => delivery_method.id,
      'adapterId' => adapter.id,
      'createdAt' => '2000-01-02T03:04:05Z',
      'updatedAt' => '2000-01-02T03:04:06Z',
      'deletedAt' => '2000-01-02T03:04:07Z'
    }
  end

  it 'renders the delivery kind attributes' do
    rendered_view

    actual_attributes = JSON.parse(rendered)
    expect(actual_attributes).to eq(expected_attributes)
  end
end
