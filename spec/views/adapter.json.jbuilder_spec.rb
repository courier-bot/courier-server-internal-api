# frozen_string_literal: true

RSpec.describe 'adapter_json_jbuilder', type: :view do
  subject(:rendered_view) { render '/adapter', record: adapter }

  let(:adapter) do
    FactoryBot.create(
      :adapter,
      code: 'adapter_code',
      uri: 'uri',
      created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
      updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
      deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
    )
  end

  let(:expected_attributes) do
    {
      'id' => adapter.id,
      'code' => 'adapter_code',
      'uri' => 'uri',
      'createdAt' => '2000-01-02T03:04:05Z',
      'updatedAt' => '2000-01-02T03:04:06Z',
      'deletedAt' => '2000-01-02T03:04:07Z'
    }
  end

  it 'renders the adapter attributes' do
    rendered_view

    actual_attributes = JSON.parse(rendered)
    expect(actual_attributes).to eq(expected_attributes)
  end
end
