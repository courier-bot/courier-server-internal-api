# frozen_string_literal: true

RSpec.describe 'delivery_json_jbuilder', type: :view do
  subject(:rendered_view) { render '/delivery', record: delivery }

  let(:delivery) do
    FactoryBot.create(
      :delivery,
      external_id: 'external_id',
      status: 'delivering',
      undelivered_reason: 'undelivered_reason',
      created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
      updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6)
    )
  end

  let(:expected_attributes) do
    {
      'id' => delivery.id,
      'messageId' => delivery.message_id,
      'deliveryKindId' => delivery.delivery_kind_id,
      'externalId' => 'external_id',
      'status' => 'delivering',
      'undeliveredReason' => 'undelivered_reason',
      'createdAt' => '2000-01-02T03:04:05Z',
      'updatedAt' => '2000-01-02T03:04:06Z'
    }
  end

  it 'renders the delivery attributes' do
    rendered_view

    actual_attributes = JSON.parse(rendered)
    expect(actual_attributes).to eq(expected_attributes)
  end
end
