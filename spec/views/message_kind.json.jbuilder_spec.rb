# frozen_string_literal: true

RSpec.describe 'message_kind_json_jbuilder', type: :view do
  subject(:rendered_view) { render '/message_kind', record: message_kind }

  let(:message_kind) do
    FactoryBot.create(
      :message_kind,
      code: 'message_kind_code',
      created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
      updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
      deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
    )
  end

  let(:expected_attributes) do
    {
      'id' => message_kind.id,
      'code' => 'message_kind_code',
      'adapterId' => message_kind.adapter_id,
      'createdAt' => '2000-01-02T03:04:05Z',
      'updatedAt' => '2000-01-02T03:04:06Z',
      'deletedAt' => '2000-01-02T03:04:07Z'
    }
  end

  it 'renders the message kind attributes' do
    rendered_view

    actual_attributes = JSON.parse(rendered)
    expect(actual_attributes).to eq(expected_attributes)
  end
end
