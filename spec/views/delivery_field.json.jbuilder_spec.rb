# frozen_string_literal: true

RSpec.describe 'delivery_field_json_jbuilder', type: :view do
  subject(:rendered_view) { render '/delivery_field', record: delivery_field }

  let(:delivery_field) do
    FactoryBot.create(
      :delivery_field,
      name: 'name',
      value: 'value',
      created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
      updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6)
    )
  end

  let(:expected_attributes) do
    {
      'id' => delivery_field.id,
      'deliveryId' => delivery_field.delivery_id,
      'name' => 'name',
      'value' => 'value',
      'createdAt' => '2000-01-02T03:04:05Z',
      'updatedAt' => '2000-01-02T03:04:06Z'
    }
  end

  it 'renders the delivery field attributes' do
    rendered_view

    actual_attributes = JSON.parse(rendered)
    expect(actual_attributes).to eq(expected_attributes)
  end
end
