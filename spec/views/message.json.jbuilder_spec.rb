# frozen_string_literal: true

RSpec.describe 'message_json_jbuilder', type: :view do
  subject(:rendered_view) { render '/message', record: message }

  let(:message) do
    FactoryBot.create(
      :message,
      target_messageable: 'target_messageable',
      status: 'delivering',
      created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
      updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6)
    )
  end

  let(:expected_attributes) do
    {
      'id' => message.id,
      'messageKindId' => message.message_kind_id,
      'targetMessageable' => 'target_messageable',
      'status' => 'delivering',
      'createdAt' => '2000-01-02T03:04:05Z',
      'updatedAt' => '2000-01-02T03:04:06Z'
    }
  end

  it 'renders the message attributes' do
    rendered_view

    actual_attributes = JSON.parse(rendered)
    expect(actual_attributes).to eq(expected_attributes)
  end
end
