# frozen_string_literal: true

RSpec.describe 'delivery_method_json_jbuilder', type: :view do
  subject(:rendered_view) { render '/delivery_method', record: delivery_method }

  let(:delivery_method) do
    FactoryBot.create(
      :delivery_method,
      code: 'delivery_method_code',
      created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
      updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
      deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
    )
  end

  let(:expected_attributes) do
    {
      'id' => delivery_method.id,
      'code' => 'delivery_method_code',
      'createdAt' => '2000-01-02T03:04:05Z',
      'updatedAt' => '2000-01-02T03:04:06Z',
      'deletedAt' => '2000-01-02T03:04:07Z'
    }
  end

  it 'renders the delivery method attributes' do
    rendered_view

    actual_attributes = JSON.parse(rendered)
    expect(actual_attributes).to eq(expected_attributes)
  end
end
