# frozen_string_literal: true

RSpec.describe DeliveryMethodsController, type: :request do
  describe 'GET /deliveryMethods' do
    subject(:request) { get '/deliveryMethods' }

    let!(:delivery_method_1) do
      FactoryBot.create(
        :delivery_method,
        code: 'delivery_method_code_1',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let!(:delivery_method_2) do
      FactoryBot.create(
        :delivery_method,
        code: 'delivery_method_code_2',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 8),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 9),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 10)
      )
    end

    let(:expected_response_body) do
      [
        {
          'id' => delivery_method_1.id,
          'code' => 'delivery_method_code_1',
          'createdAt' => '2000-01-02T03:04:05Z',
          'updatedAt' => '2000-01-02T03:04:06Z',
          'deletedAt' => '2000-01-02T03:04:07Z'
        },
        {
          'id' => delivery_method_2.id,
          'code' => 'delivery_method_code_2',
          'createdAt' => '2000-01-02T03:04:08Z',
          'updatedAt' => '2000-01-02T03:04:09Z',
          'deletedAt' => '2000-01-02T03:04:10Z'
        }
      ]
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'POST /deliveryMethods' do
    subject(:request) { post '/deliveryMethods', params: request_body }

    let(:request_body) do
      {
        code: 'delivery_method_code'
      }
    end

    let(:expected_response_body) do
      {
        'id' => kind_of(String),
        'code' => 'delivery_method_code',
        'createdAt' => Time.zone.now.iso8601,
        'updatedAt' => Time.zone.now.iso8601,
        'deletedAt' => nil
      }
    end

    let(:expected_attributes) do
      {
        code: 'delivery_method_code',
        created_at: Time.zone.now,
        updated_at: Time.zone.now,
        deleted_at: nil
      }
    end

    it 'responds with an HTTP 201 CREATED' do
      request

      expect(response).to have_http_status(:created)
    end

    it 'responds with the correct body', :freeze_time do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'creates a delivery method record' do
      expect { request }.to change(DeliveryMethod, :count).by(1)

      request
    end

    it 'creates the delivery method with the correct attributes', :freeze_time do
      request

      delivery_method = DeliveryMethod.last
      expect(delivery_method).to have_attributes(expected_attributes)
    end

    context 'when the `code` attribute is empty' do
      before { request_body[:code] = '' }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'delivery_method_code_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Delivery method attribute `code` with value `` is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end

    context 'when the `code` attribute is already taken' do
      before { FactoryBot.create(:delivery_method, code: 'delivery_method_code') }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'delivery_method_code_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Delivery method attribute `code` with value `delivery_method_code` is ' \
                        'invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end

  describe 'GET /deliveryMethods/:id' do
    subject(:request) { get "/deliveryMethods/#{requested_id}" }

    let(:requested_id) { delivery_method.id }

    let(:delivery_method) do
      FactoryBot.create(
        :delivery_method,
        code: 'delivery_method_code',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let(:expected_response_body) do
      {
        'id' => requested_id,
        'code' => 'delivery_method_code',
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => '2000-01-02T03:04:06Z',
        'deletedAt' => '2000-01-02T03:04:07Z'
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'PUT|PATCH /deliveryMethods/:id' do
    subject(:request) { put "/deliveryMethods/#{requested_id}", params: request_body }

    let(:requested_id) { delivery_method.id }

    let(:delivery_method) do
      FactoryBot.create(
        :delivery_method,
        code: 'delivery_method_code',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let(:request_body) do
      {
        code: 'updated_delivery_method_code',
        deletedAt: '2000-01-02T03:04:08Z'
      }
    end

    let(:expected_response_body) do
      {
        'id' => requested_id,
        'code' => 'updated_delivery_method_code',
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => Time.zone.now.iso8601,
        'deletedAt' => '2000-01-02T03:04:08Z'
      }
    end

    let(:expected_attributes) do
      {
        code: 'updated_delivery_method_code',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.now,
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 8)
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body', :freeze_time do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'updates the delivery method with the correct attributes', :freeze_time do
      request

      delivery_method.reload
      expect(delivery_method).to have_attributes(expected_attributes)
    end

    context 'when the `code` attribute is empty' do
      before { request_body[:code] = '' }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'delivery_method_code_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Delivery method attribute `code` with value `` is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end

    context 'when the `code` attribute is already taken' do
      before { FactoryBot.create(:delivery_method, code: 'updated_delivery_method_code') }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'delivery_method_code_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Delivery method attribute `code` with value `updated_delivery_method_code`' \
            ' is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end

  describe 'DELETE /deliveryMethods/:id' do
    subject(:request) { delete "/deliveryMethods/#{requested_id}" }

    let(:requested_id) { delivery_method.id }

    let!(:delivery_method) { FactoryBot.create(:delivery_method) }

    it 'responds with an HTTP 204 NO CONTENT' do
      request

      expect(response).to have_http_status(:no_content)
    end

    it 'removes the delivery method record' do
      expect { request }.to change(DeliveryMethod, :count).by(-1)

      request
    end

    it 'destroys the delivery method record' do
      request

      delivery_method = DeliveryMethod.exists?(id: requested_id)
      expect(delivery_method).to be_falsey
    end

    context 'when the delivery method has many delivery kinds' do
      before do
        FactoryBot.create(:delivery_kind, delivery_method: delivery_method)
      end

      let(:expected_response_body) do
        {
          'status' => '400 BAD REQUEST',
          'code' => 'delivery_method_not_deleted',
          'title' => 'Resource not deleted',
          'detail' => "Delivery method with id `#{requested_id}` could not be deleted."
        }
      end

      it 'responds with an HTTP 400 BAD REQUEST' do
        request

        expect(response).to have_http_status(:bad_request)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end
end
