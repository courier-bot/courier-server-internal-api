# frozen_string_literal: true

RSpec.describe DeliveryKindsController, type: :request do
  describe 'GET /deliveryKinds' do
    subject(:request) { get '/deliveryKinds' }

    let!(:delivery_kind_1) do
      FactoryBot.create(
        :delivery_kind,
        code: 'delivery_kind_code_1',
        delivery_method: delivery_method,
        adapter: adapter,
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let!(:delivery_kind_2) do
      FactoryBot.create(
        :delivery_kind,
        code: 'delivery_kind_code_2',
        delivery_method: delivery_method,
        adapter: adapter,
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 8),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 9),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 10)
      )
    end

    let(:delivery_method) { FactoryBot.create(:delivery_method) }

    let(:adapter) { FactoryBot.create(:adapter) }

    let(:expected_response_body) do
      [
        {
          'id' => delivery_kind_1.id,
          'code' => 'delivery_kind_code_1',
          'deliveryMethodId' => delivery_method.id,
          'adapterId' => adapter.id,
          'createdAt' => '2000-01-02T03:04:05Z',
          'updatedAt' => '2000-01-02T03:04:06Z',
          'deletedAt' => '2000-01-02T03:04:07Z'
        },
        {
          'id' => delivery_kind_2.id,
          'code' => 'delivery_kind_code_2',
          'deliveryMethodId' => delivery_method.id,
          'adapterId' => adapter.id,
          'createdAt' => '2000-01-02T03:04:08Z',
          'updatedAt' => '2000-01-02T03:04:09Z',
          'deletedAt' => '2000-01-02T03:04:10Z'
        }
      ]
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    context 'when a filter is specified' do
      subject(:request) { get '/deliveryKinds?code=delivery_kind_code_2' }

      let(:expected_response_body) do
        [
          {
            'id' => delivery_kind_2.id,
            'code' => 'delivery_kind_code_2',
            'deliveryMethodId' => delivery_method.id,
            'adapterId' => adapter.id,
            'createdAt' => '2000-01-02T03:04:08Z',
            'updatedAt' => '2000-01-02T03:04:09Z',
            'deletedAt' => '2000-01-02T03:04:10Z'
          }
        ]
      end

      it 'only retrieves the delivery kinds matching the filter' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to match(expected_response_body)
      end
    end
  end

  describe 'POST /deliveryKinds' do
    subject(:request) { post '/deliveryKinds', params: request_body }

    let(:delivery_method) { FactoryBot.create(:delivery_method) }

    let(:adapter) { FactoryBot.create(:adapter) }

    let(:request_body) do
      {
        code: 'delivery_kind_code',
        deliveryMethodId: delivery_method.id,
        adapterId: adapter.id
      }
    end

    let(:expected_response_body) do
      {
        'id' => kind_of(String),
        'code' => 'delivery_kind_code',
        'deliveryMethodId' => delivery_method.id,
        'adapterId' => adapter.id,
        'createdAt' => Time.zone.now.iso8601,
        'updatedAt' => Time.zone.now.iso8601,
        'deletedAt' => nil
      }
    end

    let(:expected_attributes) do
      {
        code: 'delivery_kind_code',
        delivery_method_id: delivery_method.id,
        adapter_id: adapter.id,
        created_at: Time.zone.now,
        updated_at: Time.zone.now,
        deleted_at: nil
      }
    end

    it 'responds with an HTTP 201 CREATED' do
      request

      expect(response).to have_http_status(:created)
    end

    it 'responds with the correct body', :freeze_time do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'creates a delivery kind record' do
      expect { request }.to change(DeliveryKind, :count).by(1)

      request
    end

    it 'creates the delivery kind with the correct attributes', :freeze_time do
      request

      delivery_kind = DeliveryKind.last
      expect(delivery_kind).to have_attributes(expected_attributes)
    end

    context 'when the `code` attribute is empty' do
      before { request_body[:code] = '' }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'delivery_kind_code_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Delivery kind attribute `code` with value `` is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end

    context 'when the `code` attribute is already taken' do
      before { FactoryBot.create(:delivery_kind, code: 'delivery_kind_code') }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'delivery_kind_code_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Delivery kind attribute `code` with value `delivery_kind_code` is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end

  describe 'GET /deliveryKinds/:id' do
    subject(:request) { get "/deliveryKinds/#{requested_id}" }

    let(:requested_id) { delivery_kind.id }

    let(:delivery_kind) do
      FactoryBot.create(
        :delivery_kind,
        code: 'delivery_kind_code',
        delivery_method: delivery_method,
        adapter: adapter,
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let(:delivery_method) { FactoryBot.create(:delivery_method) }

    let(:adapter) { FactoryBot.create(:adapter) }

    let(:expected_response_body) do
      {
        'id' => requested_id,
        'code' => 'delivery_kind_code',
        'deliveryMethodId' => delivery_method.id,
        'adapterId' => adapter.id,
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => '2000-01-02T03:04:06Z',
        'deletedAt' => '2000-01-02T03:04:07Z'
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'PUT|PATCH /deliveryKinds/:id' do
    subject(:request) { put "/deliveryKinds/#{requested_id}", params: request_body }

    let(:requested_id) { delivery_kind.id }

    let(:delivery_kind) do
      FactoryBot.create(
        :delivery_kind,
        code: 'delivery_kind_code',
        delivery_method: delivery_method,
        adapter: adapter,
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let(:delivery_method) { FactoryBot.create(:delivery_method) }

    let(:updated_delivery_method) { FactoryBot.create(:delivery_method) }

    let(:adapter) { FactoryBot.create(:adapter) }

    let(:updated_adapter) { FactoryBot.create(:adapter) }

    let(:request_body) do
      {
        code: 'updated_delivery_kind_code',
        deliveryMethodId: updated_delivery_method.id,
        adapterId: updated_adapter.id,
        deletedAt: '2000-01-02T03:04:08Z'
      }
    end

    let(:expected_response_body) do
      {
        'id' => requested_id,
        'code' => 'updated_delivery_kind_code',
        'deliveryMethodId' => updated_delivery_method.id,
        'adapterId' => updated_adapter.id,
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => Time.zone.now.iso8601,
        'deletedAt' => '2000-01-02T03:04:08Z'
      }
    end

    let(:expected_attributes) do
      {
        code: 'updated_delivery_kind_code',
        delivery_method_id: updated_delivery_method.id,
        adapter_id: updated_adapter.id,
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.now,
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 8)
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body', :freeze_time do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'updates the delivery kind with the correct attributes', :freeze_time do
      request

      delivery_kind.reload
      expect(delivery_kind).to have_attributes(expected_attributes)
    end

    context 'when the `code` attribute is empty' do
      before { request_body[:code] = '' }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'delivery_kind_code_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Delivery kind attribute `code` with value `` is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end

    context 'when the `code` attribute is already taken' do
      before { FactoryBot.create(:delivery_kind, code: 'updated_delivery_kind_code') }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'delivery_kind_code_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Delivery kind attribute `code` with value `updated_delivery_kind_code` ' \
            'is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end

  describe 'DELETE /deliveryKinds/:id' do
    subject(:request) { delete "/deliveryKinds/#{requested_id}" }

    let(:requested_id) { delivery_kind.id }

    let!(:delivery_kind) { FactoryBot.create(:delivery_kind) }

    it 'responds with an HTTP 204 NO CONTENT' do
      request

      expect(response).to have_http_status(:no_content)
    end

    it 'removes the delivery kind record' do
      expect { request }.to change(DeliveryKind, :count).by(-1)

      request
    end

    it 'destroys the delivery kind record' do
      request

      delivery_kind = DeliveryKind.exists?(id: requested_id)
      expect(delivery_kind).to be_falsey
    end

    context 'when the delivery kind has many deliveries' do
      before do
        FactoryBot.create(:delivery, delivery_kind: delivery_kind)
      end

      let(:expected_response_body) do
        {
          'status' => '400 BAD REQUEST',
          'code' => 'delivery_kind_not_deleted',
          'title' => 'Resource not deleted',
          'detail' => "Delivery kind with id `#{requested_id}` could not be deleted."
        }
      end

      it 'responds with an HTTP 400 BAD REQUEST' do
        request

        expect(response).to have_http_status(:bad_request)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end
end
