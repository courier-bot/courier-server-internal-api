# frozen_string_literal: true

RSpec.describe RecordsController, type: :request do
  # rubocop:disable RSpec/LeakyConstantDeclaration
  # Mock active controller class for testing. The `DeliveryKind` model is arbitrarily chosen, since
  # it was difficult to make everything work dynamically. In particular, partial views could not be
  # mocked in request specs, and the solution would have been to create a partial file named
  # `_mock_record.json.jbuilder` inside `app/views/` for the sole purpose of running tests.
  class MockRecordsController < DeliveryKindsController; end
  # rubocop:enable RSpec/LeakyConstantDeclaration

  Rails.application.routes.append do
    resources :mock_records, only: %i[index create show update destroy]
  end

  Rails.application.reload_routes!

  let(:mock_records) { instance_double('mock_records') }

  let(:mock_record) { FactoryBot.create(:delivery_kind) }

  describe 'GET /mock_records' do
    subject(:request) { get '/mock_records' }

    before do
      FactoryBot.create(:delivery_kind, code: 'delivery_kind_code_1')
      FactoryBot.create(:delivery_kind, code: 'delivery_kind_code_2')
      FactoryBot.create(:delivery_kind, code: 'another_code_3')
    end

    let(:mock_records) { DeliveryKind.all }

    let(:expected_response_body) do
      mock_records.map { |mock_record| mock_record_hash(mock_record) }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    context 'when a filter is specified' do
      subject(:request) { get '/mock_records?code=delivery_kind_code_2' }

      it 'only retrieves records matching the filter' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body.map { |item| item['code'] }).to be_all('delivery_kind_code_2')
      end
    end

    context 'when an order is specified' do
      subject(:request) { get '/mock_records?sort=code' }

      it 'sorts the records' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body.first['code']).to eq('another_code_3')
      end
    end
  end

  describe 'POST /mock_records' do
    subject(:request) { post '/mock_records', params: request_body }

    before do
      allow(DeliveryKind).to receive(:all).and_return(mock_records)
      allow(mock_records).to receive(:new).and_return(mock_record)
      allow(mock_record).to receive(:assign_attributes)
      allow(mock_record).to receive(:save!).and_return(true)
    end

    let(:request_body) do
      {
        code: 'code'
      }
    end

    let(:expected_response_body) { mock_record_hash(mock_record) }

    it 'creates a record with the correct attributes' do
      request

      expected_request_body = ActionController::Parameters.new(request_body).permit!
      expect(mock_record).to have_received(:assign_attributes).with(expected_request_body)
    end

    it 'saves the record to the database' do
      request

      expect(mock_record).to have_received(:save!)
    end

    it 'responds with an HTTP 201 CREATED' do
      request

      expect(response).to have_http_status(:created)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'GET /mock_records/:id' do
    subject(:request) { get "/mock_records/#{requested_id}" }

    before do
      allow(DeliveryKind).to receive(:all).and_return(mock_records)
      allow(mock_records).to receive(:find_by).and_return(mock_record)
    end

    let(:requested_id) { 'requested_id' }

    let(:expected_response_body) { mock_record_hash(mock_record) }

    it 'retrieves the record' do
      request

      expect(mock_records).to have_received(:find_by).with(id: 'requested_id')
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to eq(expected_response_body)
    end

    context 'when the requested id is invalid' do
      before do
        allow(mock_records).to receive(:find_by).and_return(nil)
      end

      let(:expected_response_body) do
        {
          'status' => '404 NOT FOUND',
          'code' => 'delivery_kind_not_found',
          'title' => 'Resource not found',
          'detail' => 'Delivery kind with id `requested_id` could not be found.'
        }
      end

      it 'responds with an HTTP 404 NOT FOUND' do
        request

        expect(response).to have_http_status(:not_found)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end

  describe 'PUT|PATCH /mock_records/:id' do
    subject(:request) { put "/mock_records/#{requested_id}", params: request_body }

    before do
      allow(DeliveryKind).to receive(:all).and_return(mock_records)
      allow(mock_records).to receive(:find_by).and_return(mock_record)
      allow(mock_record).to receive(:assign_attributes)
      allow(mock_record).to receive(:save!).and_return(true)
    end

    let(:requested_id) { 'requested_id' }

    let(:request_body) do
      {
        'code' => 'updated_code'
      }
    end

    let(:expected_response_body) { mock_record_hash(mock_record) }

    it 'updates the record with the correct attributes' do
      request

      expect(mock_record)
        .to have_received(:assign_attributes).with(request_body.transform_keys(&:underscore))
    end

    it 'saves the record to the database' do
      request

      expect(mock_record).to have_received(:save!)
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to eq(expected_response_body)
    end

    context 'when the requested id is invalid' do
      before do
        allow(mock_records).to receive(:find_by).and_return(nil)
      end

      let(:expected_response_body) do
        {
          'status' => '404 NOT FOUND',
          'code' => 'delivery_kind_not_found',
          'title' => 'Resource not found',
          'detail' => 'Delivery kind with id `requested_id` could not be found.'
        }
      end

      it 'responds with an HTTP 404 NOT FOUND' do
        request

        expect(response).to have_http_status(:not_found)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end

  describe 'DELETE /mock_records/:id' do
    subject(:request) { delete "/mock_records/#{requested_id}" }

    before do
      allow(DeliveryKind).to receive(:all).and_return(mock_records)
      allow(mock_records).to receive(:find_by).and_return(mock_record)
      allow(mock_record).to receive(:destroy!).and_return(true)
    end

    let(:requested_id) { 'requested_id' }

    it 'destroys the record' do
      request

      expect(mock_record).to have_received(:destroy!)
    end

    it 'responds with an HTTP 204 NO CONTENT' do
      request

      expect(response).to have_http_status(:no_content)
    end

    context 'when the record cannot be deleted' do
      before do
        allow(mock_record).to receive(:destroy!).and_raise(ActiveRecord::InvalidForeignKey)
      end

      let(:expected_response_body) do
        {
          'status' => '400 BAD REQUEST',
          'code' => 'delivery_kind_not_deleted',
          'title' => 'Resource not deleted',
          'detail' => 'Delivery kind with id `requested_id` could not be deleted.'
        }
      end

      it 'responds with an HTTP 400 BAD REQUEST' do
        request

        expect(response).to have_http_status(:bad_request)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end

    context 'when the requested id is invalid' do
      before do
        allow(mock_records).to receive(:find_by).and_return(nil)
      end

      let(:expected_response_body) do
        {
          'status' => '404 NOT FOUND',
          'code' => 'delivery_kind_not_found',
          'title' => 'Resource not found',
          'detail' => 'Delivery kind with id `requested_id` could not be found.'
        }
      end

      it 'responds with an HTTP 404 NOT FOUND' do
        request

        expect(response).to have_http_status(:not_found)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end

  def mock_record_hash(mock_record)
    hash = mock_record.attributes

    hash['created_at'] = hash['created_at'].iso8601
    hash['updated_at'] = hash['updated_at'].iso8601

    hash.deep_transform_keys { |k| k.camelcase(:lower) }
  end
end
