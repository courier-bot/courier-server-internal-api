# frozen_string_literal: true

RSpec.describe DeliveriesController, type: :request do
  describe 'GET /deliveries' do
    subject(:request) { get '/deliveries' }

    let!(:delivery_1) do
      FactoryBot.create(
        :delivery,
        external_id: 'external_id_1',
        status: 'delivering',
        undelivered_reason: 'undelivered_reason_1',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6)
      )
    end

    let!(:delivery_2) do
      FactoryBot.create(
        :delivery,
        external_id: 'external_id_2',
        status: 'delivered',
        undelivered_reason: 'undelivered_reason_2',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 7),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 8)
      )
    end

    let(:expected_response_body) do
      [
        {
          'id' => delivery_1.id,
          'messageId' => delivery_1.message_id,
          'deliveryKindId' => delivery_1.delivery_kind_id,
          'externalId' => 'external_id_1',
          'status' => 'delivering',
          'undeliveredReason' => 'undelivered_reason_1',
          'createdAt' => '2000-01-02T03:04:05Z',
          'updatedAt' => '2000-01-02T03:04:06Z'
        },
        {
          'id' => delivery_2.id,
          'messageId' => delivery_2.message_id,
          'deliveryKindId' => delivery_2.delivery_kind_id,
          'externalId' => 'external_id_2',
          'status' => 'delivered',
          'undeliveredReason' => 'undelivered_reason_2',
          'createdAt' => '2000-01-02T03:04:07Z',
          'updatedAt' => '2000-01-02T03:04:08Z'
        }
      ]
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'POST /deliveries' do
    subject(:request) { post '/deliveries', params: request_body }

    let!(:message) { FactoryBot.create(:message) }

    let!(:delivery_kind) { FactoryBot.create(:delivery_kind) }

    let(:request_body) do
      {
        messageId: message.id,
        deliveryKindId: delivery_kind.id,
        externalId: 'external_id',
        status: 'delivering'
      }
    end

    let(:expected_response_body) do
      {
        'id' => kind_of(String),
        'messageId' => message.id,
        'deliveryKindId' => delivery_kind.id,
        'externalId' => 'external_id',
        'status' => 'delivering',
        'undeliveredReason' => nil,
        'createdAt' => Time.zone.now.iso8601,
        'updatedAt' => Time.zone.now.iso8601
      }
    end

    let(:expected_attributes) do
      {
        message_id: message.id,
        delivery_kind_id: delivery_kind.id,
        external_id: 'external_id',
        status: 'delivering',
        undelivered_reason: nil,
        created_at: Time.zone.now,
        updated_at: Time.zone.now
      }
    end

    it 'responds with an HTTP 201 CREATED' do
      request

      expect(response).to have_http_status(:created)
    end

    it 'responds with the correct body', :freeze_time do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'creates a delivery record' do
      expect { request }.to change(Delivery, :count).by(1)

      request
    end

    it 'creates the delivery with the correct attributes', :freeze_time do
      request

      delivery = Delivery.last
      expect(delivery).to have_attributes(expected_attributes)
    end

    context 'when the `status` enum is invalid' do
      before { request_body[:status] = 'invalid' }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'delivery_status_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Delivery attribute `status` with value `invalid` is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end

  describe 'GET /deliveries/:id' do
    subject(:request) { get "/deliveries/#{requested_id}" }

    let(:requested_id) { delivery.id }

    let(:delivery) do
      FactoryBot.create(
        :delivery,
        external_id: 'external_id',
        status: 'delivering',
        undelivered_reason: 'undelivered_reason',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6)
      )
    end

    let(:expected_response_body) do
      {
        'id' => requested_id,
        'messageId' => delivery.message_id,
        'deliveryKindId' => delivery.delivery_kind_id,
        'externalId' => 'external_id',
        'status' => 'delivering',
        'undeliveredReason' => 'undelivered_reason',
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => '2000-01-02T03:04:06Z'
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'PUT|PATCH /deliveries/:id' do
    subject(:request) { put "/deliveries/#{requested_id}", params: request_body }

    let(:requested_id) { delivery.id }

    let(:delivery) do
      FactoryBot.create(
        :delivery,
        external_id: 'external_id',
        status: 'delivering',
        undelivered_reason: 'undelivered_reason',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6)
      )
    end

    let(:updated_delivery_kind) { FactoryBot.create(:delivery_kind) }

    let(:request_body) do
      {
        deliveryKindId: updated_delivery_kind.id,
        externalId: 'updated_external_id',
        status: 'delivered',
        undeliveredReason: 'updated_undelivered_reason'
      }
    end

    let(:expected_response_body) do
      {
        'id' => delivery.id,
        'messageId' => delivery.message_id,
        'deliveryKindId' => updated_delivery_kind.id,
        'externalId' => 'updated_external_id',
        'status' => 'delivered',
        'undeliveredReason' => 'updated_undelivered_reason',
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => Time.zone.now.iso8601
      }
    end

    let(:expected_attributes) do
      {
        message_id: delivery.message_id,
        delivery_kind_id: updated_delivery_kind.id,
        external_id: 'updated_external_id',
        status: 'delivered',
        undelivered_reason: 'updated_undelivered_reason',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.now
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body', :freeze_time do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'updates the delivery with the correct attributes', :freeze_time do
      request

      delivery.reload
      expect(delivery).to have_attributes(expected_attributes)
    end

    context 'when the `status` enum is invalid' do
      before { request_body[:status] = 'invalid' }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'delivery_status_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Delivery attribute `status` with value `invalid` is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end

  describe 'DELETE /deliveries/:id' do
    subject(:request) { delete "/deliveries/#{requested_id}" }

    let(:requested_id) { delivery.id }

    let!(:delivery) { FactoryBot.create(:delivery) }

    it 'responds with an HTTP 204 NO CONTENT' do
      request

      expect(response).to have_http_status(:no_content)
    end

    it 'removes the delivery record' do
      expect { request }.to change(Delivery, :count).by(-1)

      request
    end

    it 'destroys the delivery record' do
      request

      delivery = Delivery.exists?(id: requested_id)
      expect(delivery).to be_falsey
    end
  end
end
