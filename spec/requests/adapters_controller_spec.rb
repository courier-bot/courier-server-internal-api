# frozen_string_literal: true

RSpec.describe AdaptersController, type: :request do
  describe 'GET /adapters' do
    subject(:request) { get '/adapters' }

    let!(:adapter_1) do
      FactoryBot.create(
        :adapter,
        code: 'adapter_code_1',
        uri: 'uri_1',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let!(:adapter_2) do
      FactoryBot.create(
        :adapter,
        code: 'adapter_code_2',
        uri: 'uri_2',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 8),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 9),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 10)
      )
    end

    let(:expected_response_body) do
      [
        {
          'id' => adapter_1.id,
          'code' => 'adapter_code_1',
          'uri' => 'uri_1',
          'createdAt' => '2000-01-02T03:04:05Z',
          'updatedAt' => '2000-01-02T03:04:06Z',
          'deletedAt' => '2000-01-02T03:04:07Z'
        },
        {
          'id' => adapter_2.id,
          'code' => 'adapter_code_2',
          'uri' => 'uri_2',
          'createdAt' => '2000-01-02T03:04:08Z',
          'updatedAt' => '2000-01-02T03:04:09Z',
          'deletedAt' => '2000-01-02T03:04:10Z'
        }
      ]
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'POST /adapters' do
    subject(:request) { post '/adapters', params: request_body }

    let(:request_body) do
      {
        code: 'adapter_code',
        uri: 'uri'
      }
    end

    let(:expected_response_body) do
      {
        'id' => kind_of(String),
        'code' => 'adapter_code',
        'uri' => 'uri',
        'createdAt' => Time.zone.now.iso8601,
        'updatedAt' => Time.zone.now.iso8601,
        'deletedAt' => nil
      }
    end

    let(:expected_attributes) do
      {
        code: 'adapter_code',
        uri: 'uri',
        created_at: Time.zone.now,
        updated_at: Time.zone.now,
        deleted_at: nil
      }
    end

    it 'responds with an HTTP 201 CREATED' do
      request

      expect(response).to have_http_status(:created)
    end

    it 'responds with the correct body', :freeze_time do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'creates a adapter record' do
      expect { request }.to change(Adapter, :count).by(1)

      request
    end

    it 'creates the adapter with the correct attributes', :freeze_time do
      request

      adapter = Adapter.last
      expect(adapter).to have_attributes(expected_attributes)
    end

    context 'when the `code` attribute is empty' do
      before { request_body[:code] = '' }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'adapter_code_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Adapter attribute `code` with value `` is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end

    context 'when the `code` attribute is already taken' do
      before { FactoryBot.create(:adapter, code: 'adapter_code') }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'adapter_code_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Adapter attribute `code` with value `adapter_code` is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end

  describe 'GET /adapters/:id' do
    subject(:request) { get "/adapters/#{requested_id}" }

    let(:requested_id) { adapter.id }

    let(:adapter) do
      FactoryBot.create(
        :adapter,
        code: 'adapter_code',
        uri: 'uri',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let(:expected_response_body) do
      {
        'id' => requested_id,
        'code' => 'adapter_code',
        'uri' => 'uri',
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => '2000-01-02T03:04:06Z',
        'deletedAt' => '2000-01-02T03:04:07Z'
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'PUT|PATCH /adapters/:id' do
    subject(:request) { put "/adapters/#{requested_id}", params: request_body }

    let(:requested_id) { adapter.id }

    let(:adapter) do
      FactoryBot.create(
        :adapter,
        code: 'adapter_code',
        uri: 'uri',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let(:request_body) do
      {
        code: 'updated_adapter_code',
        uri: 'updated_uri',
        deletedAt: '2000-01-02T03:04:08Z'
      }
    end

    let(:expected_response_body) do
      {
        'id' => requested_id,
        'code' => 'updated_adapter_code',
        'uri' => 'updated_uri',
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => Time.zone.now.iso8601,
        'deletedAt' => '2000-01-02T03:04:08Z'
      }
    end

    let(:expected_attributes) do
      {
        code: 'updated_adapter_code',
        uri: 'updated_uri',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.now,
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 8)
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body', :freeze_time do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'updates the adapter with the correct attributes', :freeze_time do
      request

      adapter.reload
      expect(adapter).to have_attributes(expected_attributes)
    end

    context 'when the `code` attribute is empty' do
      before { request_body[:code] = '' }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'adapter_code_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Adapter attribute `code` with value `` is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end

    context 'when the `code` attribute is already taken' do
      before { FactoryBot.create(:adapter, code: 'updated_adapter_code') }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'adapter_code_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Adapter attribute `code` with value `updated_adapter_code` is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end

  describe 'DELETE /adapters/:id' do
    subject(:request) { delete "/adapters/#{requested_id}" }

    let(:requested_id) { adapter.id }

    let!(:adapter) { FactoryBot.create(:adapter) }

    it 'responds with an HTTP 204 NO CONTENT' do
      request

      expect(response).to have_http_status(:no_content)
    end

    it 'removes the adapter record' do
      expect { request }.to change(Adapter, :count).by(-1)

      request
    end

    it 'destroys the adapter record' do
      request

      adapter = Adapter.exists?(id: requested_id)
      expect(adapter).to be_falsey
    end

    context 'when the adapter has many message kinds' do
      before do
        FactoryBot.create(:message_kind, adapter: adapter)
      end

      let(:expected_response_body) do
        {
          'status' => '400 BAD REQUEST',
          'code' => 'adapter_not_deleted',
          'title' => 'Resource not deleted',
          'detail' => "Adapter with id `#{requested_id}` could not be deleted."
        }
      end

      it 'responds with an HTTP 400 BAD REQUEST' do
        request

        expect(response).to have_http_status(:bad_request)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end
end
