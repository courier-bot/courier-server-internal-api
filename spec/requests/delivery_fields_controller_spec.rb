# frozen_string_literal: true

RSpec.describe DeliveryFieldsController, type: :request do
  let(:delivery_id) { delivery.id }

  let(:delivery) { FactoryBot.create(:delivery) }

  describe 'GET /deliveryFields' do
    subject(:request) { get '/deliveryFields' }

    let!(:delivery_field_1) do
      FactoryBot.create(
        :delivery_field,
        delivery: delivery,
        name: 'name_1',
        value: 'value_1',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6)
      )
    end

    let!(:delivery_field_2) do
      FactoryBot.create(
        :delivery_field,
        name: 'name_2',
        value: 'value_2',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 7),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 8)
      )
    end

    let(:expected_response_body) do
      [
        {
          'id' => delivery_field_1.id,
          'deliveryId' => delivery_id,
          'name' => 'name_1',
          'value' => 'value_1',
          'createdAt' => '2000-01-02T03:04:05Z',
          'updatedAt' => '2000-01-02T03:04:06Z'
        },
        {
          'id' => delivery_field_2.id,
          'deliveryId' => kind_of(String),
          'name' => 'name_2',
          'value' => 'value_2',
          'createdAt' => '2000-01-02T03:04:07Z',
          'updatedAt' => '2000-01-02T03:04:08Z'
        }
      ]
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'POST /deliveryFields' do
    subject(:request) { post '/deliveryFields', params: request_body }

    let(:request_body) do
      {
        deliveryId: delivery_id,
        name: 'name',
        value: 'value'
      }
    end

    let(:expected_response_body) do
      {
        'id' => kind_of(String),
        'deliveryId' => delivery_id,
        'name' => 'name',
        'value' => 'value',
        'createdAt' => Time.zone.now.iso8601,
        'updatedAt' => Time.zone.now.iso8601
      }
    end

    let(:expected_attributes) do
      {
        delivery_id: delivery_id,
        name: 'name',
        value: 'value',
        created_at: Time.zone.now,
        updated_at: Time.zone.now
      }
    end

    it 'responds with an HTTP 201 CREATED' do
      request

      expect(response).to have_http_status(:created)
    end

    it 'responds with the correct body', :freeze_time do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'creates a delivery field record' do
      expect { request }.to change(DeliveryField, :count).by(1)

      request
    end

    it 'creates the delivery field with the correct attributes', :freeze_time do
      request

      delivery_field = DeliveryField.last
      expect(delivery_field).to have_attributes(expected_attributes)
    end
  end

  describe 'GET /deliveryFields/:id' do
    subject(:request) { get "/deliveryFields/#{requested_id}" }

    let(:requested_id) { delivery_field.id }

    let(:delivery_field) do
      FactoryBot.create(
        :delivery_field,
        delivery: delivery,
        name: 'name',
        value: 'value',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6)
      )
    end

    let(:expected_response_body) do
      {
        'id' => requested_id,
        'deliveryId' => delivery_id,
        'name' => 'name',
        'value' => 'value',
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => '2000-01-02T03:04:06Z'
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'PUT|PATCH /deliveryFields/:id' do
    subject(:request) { put "/deliveryFields/#{requested_id}", params: request_body }

    let(:requested_id) { delivery_field.id }

    let(:delivery_field) do
      FactoryBot.create(
        :delivery_field,
        delivery: delivery,
        name: 'name',
        value: 'value',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6)
      )
    end

    let(:request_body) do
      {
        value: 'updated_value'
      }
    end

    let(:expected_response_body) do
      {
        'id' => requested_id,
        'deliveryId' => delivery_id,
        'name' => 'name',
        'value' => 'updated_value',
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => Time.zone.now.iso8601
      }
    end

    let(:expected_attributes) do
      {
        delivery_id: delivery_id,
        name: 'name',
        value: 'updated_value',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.now
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body', :freeze_time do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'updates the delivery field with the correct attributes', :freeze_time do
      request

      delivery_field.reload
      expect(delivery_field).to have_attributes(expected_attributes)
    end
  end

  describe 'DELETE /deliveryFields/:id' do
    subject(:request) { delete "/deliveryFields/#{requested_id}" }

    let(:requested_id) { delivery_field.id }

    let!(:delivery_field) { FactoryBot.create(:delivery_field, delivery: delivery) }

    it 'responds with an HTTP 204 NO CONTENT' do
      request

      expect(response).to have_http_status(:no_content)
    end

    it 'removes the delivery field record' do
      expect { request }.to change(DeliveryField, :count).by(-1)

      request
    end

    it 'destroys the delivery field record' do
      request

      delivery_field = DeliveryField.exists?(id: requested_id)
      expect(delivery_field).to be_falsey
    end
  end
end
