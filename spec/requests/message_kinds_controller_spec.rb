# frozen_string_literal: true

RSpec.describe MessageKindsController, type: :request do
  describe 'GET /messageKinds' do
    subject(:request) { get '/messageKinds' }

    let!(:message_kind_1) do
      FactoryBot.create(
        :message_kind,
        code: 'message_kind_code_1',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let!(:message_kind_2) do
      FactoryBot.create(
        :message_kind,
        code: 'message_kind_code_2',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 8),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 9),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 10)
      )
    end

    let(:expected_response_body) do
      [
        {
          'id' => message_kind_1.id,
          'code' => 'message_kind_code_1',
          'adapterId' => message_kind_1.adapter_id,
          'createdAt' => '2000-01-02T03:04:05Z',
          'updatedAt' => '2000-01-02T03:04:06Z',
          'deletedAt' => '2000-01-02T03:04:07Z'
        },
        {
          'id' => message_kind_2.id,
          'code' => 'message_kind_code_2',
          'adapterId' => message_kind_2.adapter_id,
          'createdAt' => '2000-01-02T03:04:08Z',
          'updatedAt' => '2000-01-02T03:04:09Z',
          'deletedAt' => '2000-01-02T03:04:10Z'
        }
      ]
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'POST /messageKinds' do
    subject(:request) { post '/messageKinds', params: request_body }

    let!(:adapter) { FactoryBot.create(:adapter) }

    let(:request_body) do
      {
        code: 'message_kind_code',
        adapterId: adapter.id
      }
    end

    let(:expected_response_body) do
      {
        'id' => kind_of(String),
        'code' => 'message_kind_code',
        'adapterId' => adapter.id,
        'createdAt' => Time.zone.now.iso8601,
        'updatedAt' => Time.zone.now.iso8601,
        'deletedAt' => nil
      }
    end

    let(:expected_attributes) do
      {
        code: 'message_kind_code',
        adapter_id: adapter.id,
        created_at: Time.zone.now,
        updated_at: Time.zone.now,
        deleted_at: nil
      }
    end

    it 'responds with an HTTP 201 CREATED' do
      request

      expect(response).to have_http_status(:created)
    end

    it 'responds with the correct body', :freeze_time do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'creates a message kind record' do
      expect { request }.to change(MessageKind, :count).by(1)

      request
    end

    it 'creates the message kind with the correct attributes', :freeze_time do
      request

      message_kind = MessageKind.last
      expect(message_kind).to have_attributes(expected_attributes)
    end

    context 'when the `code` attribute is empty' do
      before { request_body[:code] = '' }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'message_kind_code_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Message kind attribute `code` with value `` is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end

    context 'when the `code` attribute is already taken' do
      before { FactoryBot.create(:message_kind, code: 'message_kind_code') }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'message_kind_code_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Message kind attribute `code` with value `message_kind_code` is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end

  describe 'GET /messageKinds/:id' do
    subject(:request) { get "/messageKinds/#{requested_id}" }

    let(:requested_id) { message_kind.id }

    let(:message_kind) do
      FactoryBot.create(
        :message_kind,
        code: 'message_kind_code',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let(:expected_response_body) do
      {
        'id' => requested_id,
        'code' => 'message_kind_code',
        'adapterId' => message_kind.adapter_id,
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => '2000-01-02T03:04:06Z',
        'deletedAt' => '2000-01-02T03:04:07Z'
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'PUT|PATCH /messageKinds/:id' do
    subject(:request) { put "/messageKinds/#{requested_id}", params: request_body }

    let(:requested_id) { message_kind.id }

    let(:message_kind) do
      FactoryBot.create(
        :message_kind,
        code: 'message_kind_code',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6),
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 7)
      )
    end

    let(:updated_adapter) { FactoryBot.create(:adapter) }

    let(:request_body) do
      {
        code: 'updated_message_kind_code',
        adapterId: updated_adapter.id,
        deletedAt: '2000-01-02T03:04:08Z'
      }
    end

    let(:expected_response_body) do
      {
        'id' => requested_id,
        'code' => 'updated_message_kind_code',
        'adapterId' => updated_adapter.id,
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => Time.zone.now.iso8601,
        'deletedAt' => '2000-01-02T03:04:08Z'
      }
    end

    let(:expected_attributes) do
      {
        code: 'updated_message_kind_code',
        adapter_id: updated_adapter.id,
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.now,
        deleted_at: Time.zone.local(2000, 1, 2, 3, 4, 8)
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body', :freeze_time do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'updates the message kind with the correct attributes', :freeze_time do
      request

      message_kind.reload
      expect(message_kind).to have_attributes(expected_attributes)
    end

    context 'when the `code` attribute is empty' do
      before { request_body[:code] = '' }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'message_kind_code_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Message kind attribute `code` with value `` is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end

    context 'when the `code` attribute is already taken' do
      before { FactoryBot.create(:message_kind, code: 'updated_message_kind_code') }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'message_kind_code_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Message kind attribute `code` with value `updated_message_kind_code` is ' \
            'invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end

  describe 'DELETE /messageKinds/:id' do
    subject(:request) { delete "/messageKinds/#{requested_id}" }

    let(:requested_id) { message_kind.id }

    let!(:message_kind) { FactoryBot.create(:message_kind) }

    it 'responds with an HTTP 204 NO CONTENT' do
      request

      expect(response).to have_http_status(:no_content)
    end

    it 'removes the message kind record' do
      expect { request }.to change(MessageKind, :count).by(-1)

      request
    end

    it 'destroys the message kind record' do
      request

      message_kind = MessageKind.exists?(id: requested_id)
      expect(message_kind).to be_falsey
    end

    context 'when the message kind has many messages' do
      before do
        FactoryBot.create(:message, message_kind: message_kind)
      end

      let(:expected_response_body) do
        {
          'status' => '400 BAD REQUEST',
          'code' => 'message_kind_not_deleted',
          'title' => 'Resource not deleted',
          'detail' => "Message kind with id `#{requested_id}` could not be deleted."
        }
      end

      it 'responds with an HTTP 400 BAD REQUEST' do
        request

        expect(response).to have_http_status(:bad_request)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end
end
