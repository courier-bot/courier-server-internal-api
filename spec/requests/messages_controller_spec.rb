# frozen_string_literal: true

RSpec.describe MessagesController, type: :request do
  describe 'GET /messages' do
    subject(:request) { get '/messages' }

    let!(:message_1) do
      FactoryBot.create(
        :message,
        target_messageable: 'target_messageable_1',
        status: 'delivering',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6)
      )
    end

    let!(:message_2) do
      FactoryBot.create(
        :message,
        target_messageable: 'target_messageable_2',
        status: 'delivered',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 7),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 8)
      )
    end

    let(:expected_response_body) do
      [
        {
          'id' => message_1.id,
          'messageKindId' => message_1.message_kind_id,
          'targetMessageable' => 'target_messageable_1',
          'status' => 'delivering',
          'createdAt' => '2000-01-02T03:04:05Z',
          'updatedAt' => '2000-01-02T03:04:06Z'
        },
        {
          'id' => message_2.id,
          'messageKindId' => message_2.message_kind_id,
          'targetMessageable' => 'target_messageable_2',
          'status' => 'delivered',
          'createdAt' => '2000-01-02T03:04:07Z',
          'updatedAt' => '2000-01-02T03:04:08Z'
        }
      ]
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'POST /messages' do
    subject(:request) { post '/messages', params: request_body }

    let!(:message_kind) { FactoryBot.create(:message_kind) }

    let(:request_body) do
      {
        messageKindId: message_kind.id,
        targetMessageable: 'target_messageable',
        status: 'delivering'
      }
    end

    let(:expected_response_body) do
      {
        'id' => kind_of(String),
        'messageKindId' => message_kind.id,
        'targetMessageable' => 'target_messageable',
        'status' => 'delivering',
        'createdAt' => Time.zone.now.iso8601,
        'updatedAt' => Time.zone.now.iso8601
      }
    end

    let(:expected_attributes) do
      {
        message_kind_id: message_kind.id,
        target_messageable: 'target_messageable',
        status: 'delivering',
        created_at: Time.zone.now,
        updated_at: Time.zone.now
      }
    end

    it 'responds with an HTTP 201 CREATED' do
      request

      expect(response).to have_http_status(:created)
    end

    it 'responds with the correct body', :freeze_time do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'creates a message record' do
      expect { request }.to change(Message, :count).by(1)

      request
    end

    it 'creates the message with the correct attributes', :freeze_time do
      request

      message = Message.last
      expect(message).to have_attributes(expected_attributes)
    end

    context 'when the `status` enum is invalid' do
      before { request_body[:status] = 'invalid' }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'message_status_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Message attribute `status` with value `invalid` is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end

  describe 'GET /messages/:id' do
    subject(:request) { get "/messages/#{requested_id}" }

    let(:requested_id) { message.id }

    let(:message) do
      FactoryBot.create(
        :message,
        target_messageable: 'target_messageable',
        status: 'delivering',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6)
      )
    end

    let(:expected_response_body) do
      {
        'id' => requested_id,
        'messageKindId' => message.message_kind_id,
        'targetMessageable' => 'target_messageable',
        'status' => 'delivering',
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => '2000-01-02T03:04:06Z'
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'PUT|PATCH /messages/:id' do
    subject(:request) { put "/messages/#{requested_id}", params: request_body }

    let(:requested_id) { message.id }

    let(:message) do
      FactoryBot.create(
        :message,
        target_messageable: 'target_messageable',
        status: 'delivering',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.local(2000, 1, 2, 3, 4, 6)
      )
    end

    let(:updated_message_kind) { FactoryBot.create(:message_kind) }

    let(:request_body) do
      {
        messageKindId: updated_message_kind.id,
        targetMessageable: 'updated_target_messageable',
        status: 'delivered'
      }
    end

    let(:expected_response_body) do
      {
        'id' => requested_id,
        'messageKindId' => updated_message_kind.id,
        'targetMessageable' => 'updated_target_messageable',
        'status' => 'delivered',
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => Time.zone.now.iso8601
      }
    end

    let(:expected_attributes) do
      {
        message_kind_id: updated_message_kind.id,
        target_messageable: 'updated_target_messageable',
        status: 'delivered',
        created_at: Time.zone.local(2000, 1, 2, 3, 4, 5),
        updated_at: Time.zone.now
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body', :freeze_time do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'updates the message with the correct attributes', :freeze_time do
      request

      message.reload
      expect(message).to have_attributes(expected_attributes)
    end

    context 'when the `status` enum is invalid' do
      before { request_body[:status] = 'invalid' }

      let(:expected_response_body) do
        {
          'status' => '422 UNPROCESSABLE ENTITY',
          'code' => 'message_status_invalid',
          'title' => 'Invalid attribute value',
          'detail' => 'Message attribute `status` with value `invalid` is invalid.'
        }
      end

      it 'responds with an HTTP 422 BAD REQUEST' do
        request

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the correct body' do
        request

        response_body = JSON.parse(response.body)
        expect(response_body).to eq(expected_response_body)
      end
    end
  end

  describe 'DELETE /messages/:id' do
    subject(:request) { delete "/messages/#{requested_id}" }

    let(:requested_id) { message.id }

    let!(:message) { FactoryBot.create(:message) }

    it 'responds with an HTTP 204 NO CONTENT' do
      request

      expect(response).to have_http_status(:no_content)
    end

    it 'removes the message record' do
      expect { request }.to change(Message, :count).by(-1)

      request
    end

    it 'destroys the message record' do
      request

      message = Message.exists?(id: requested_id)
      expect(message).to be_falsey
    end
  end
end
